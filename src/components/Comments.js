import { useState, useContext } from "react";
import moment from "moment";
import "./comments.css";
import UserContext from "../UserContext";
import { motion } from "framer-motion";
export default function Comment({
  comments,
  handleSubmitUpdate,
  handleDelete,
}) {
  const { user } = useContext(UserContext);
  const { comment, username, _id, commentedOn } = comments;
  const initialState = {
    comment: comment,
  };

  const [formValue, setFormValue] = useState(initialState);
  const [toggle, setToggle] = useState(false);
  const [toggleEdit, setToggleEdit] = useState(false);
  const [toggleSave, setToggleSave] = useState(false);

  function handleToggle() {
    setToggle((prevState) => !prevState);
  }
  function handleEdit() {
    setToggleEdit((prevState) => !prevState);
    setToggle(false);
  }

  function handleChange(e) {
    const { name, value } = e.target;
    setFormValue({ ...formValue, [name]: value });

    if (formValue.comment !== initialState) {
      setToggleSave(true);
    }
  }

  return (
    <motion.div
      className="comments"
      initial="hidden"
      whileInView="visible"
      viewport={{ once: true }}
      transition={{ duration: 0.4 }}
      whileHover={{ scale: 1.0025 }}
      whileTap={{ scale: 0.9975 }}
      style={{ x: 0 }}
    >
      <div className="comment-author">
        <p>{username}</p>
        <div className="comment-action">
          <i className="bx bx-dots-vertical-rounded" onClick={handleToggle}></i>
          {toggle ? (
            <div className="toggle-action">
              <div className="delete" onClick={() => handleDelete(_id)}>
                <i className="bx bxs-trash-alt"></i>
                <span>Delete</span>
              </div>
              <div className="edit" onClick={handleEdit}>
                <i className="bx bxs-pencil"></i>
                <span>Edit</span>
              </div>
            </div>
          ) : (
            <></>
          )}
        </div>
      </div>
      <div className="comment-description">
        {toggleEdit ? (
          <form
            onSubmit={(e) => {
              handleSubmitUpdate(e, _id, formValue.comment);
              handleEdit();
            }}
          >
            <input
              type="text"
              maxLength={55}
              value={formValue.comment}
              name="comment"
              onChange={handleChange}
            />
            <div className="edit-action">
              <button type="button" className="cancel" onClick={handleEdit}>
                Cancel
              </button>
              {toggleSave ? (
                <button type="submit" className="save">
                  Save
                </button>
              ) : (
                <button type="submit" className="save-disabled" disabled>
                  Save
                </button>
              )}
            </div>
          </form>
        ) : (
          <p>{comment}</p>
        )}
        <span>{moment(commentedOn).fromNow()}</span>
      </div>
    </motion.div>
  );
}
