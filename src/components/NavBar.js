import "./NavBar.css";
import { NavLink } from "react-router-dom";
import UserContext from "../UserContext";
import { useContext } from "react";

export default function NavBar() {
  const { user } = useContext(UserContext);

  return (
    <>
      <nav>
        <div className="logo">
          <h3 className="logo">logs</h3>
        </div>

        <ul className="nav-links">
          {user.id === null ? (
            <></>
          ) : user.isAdmin ? (
            <>
              <NavLink to="/dashboard">Dashboard</NavLink>
              <NavLink to="/logout">Logout</NavLink>
            </>
          ) : (
            <>
              <NavLink to="/blogs">Blogs</NavLink>
              <NavLink to="/logout">Logout</NavLink>
            </>
          )}
        </ul>
      </nav>
    </>
  );
}
