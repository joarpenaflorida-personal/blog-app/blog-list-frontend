import "./bloglist.css";
import { useContext } from "react";
import UserContext from "../UserContext";
import moment from "moment";
import { Link } from "react-router-dom";
import { motion } from "framer-motion";
export default function UserBlogs({ blogs, deleteBlog, updateBlog, archive }) {
  const { user } = useContext(UserContext);

  const {
    _id,
    title,
    readTime,
    slug,
    likes,
    comments,
    tag,
    author,
    postedOn,
    isActive,
  } = blogs;

  console.log(comments.length);
  console.log(likes.length);
  return (
    <motion.div
      className="blog-list"
      transition={{ duration: 0.4 }}
      whileHover={{ scale: 1.0025 }}
      whileTap={{ scale: 0.9975 }}
      style={{ x: 0 }}
    >
      <div className="comment-bottom">
        {blogs.isActive ? (
          <>
            <div
              className="blog-delete"
              onClick={(e) => archive(_id, isActive)}
            >
              <i className="bx bxs-archive-in"></i>
              <span>Archive</span>
            </div>
          </>
        ) : (
          <>
            <div
              className="blog-delete"
              onClick={(e) => archive(_id, isActive)}
            >
              <i className="bx bx-archive-out"></i>
              <span>Unarchive</span>
            </div>
            <div className="blog-delete" onClick={(e) => deleteBlog(_id)}>
              <i className="bx bxs-trash-alt"></i>

              <span>Delete</span>
            </div>
            <div className="blog-update" onClick={(e) => updateBlog(slug)}>
              <i className="bx bxs-pencil"></i>

              <span>Edit</span>
            </div>
          </>
        )}
      </div>
      <div className="blog-header">
        <h3 className="blog-title">{title}</h3>
        <span className="author">{author}</span>
      </div>
      <div className="blog-details">
        <div className="blog-tags">
          <span className="blog-tag">{tag}</span>
          <p className="blog-time">{readTime}</p>
        </div>
        <div className="blog-date">{moment(postedOn).fromNow()}</div>
      </div>
    </motion.div>
  );
}
