import "./bloglist.css";
import { useContext } from "react";
import UserContext from "../UserContext";
import moment from "moment";
import { Link } from "react-router-dom";
import { motion } from "framer-motion";
export default function BlogList({ blogs, toggleLike }) {
  const { user } = useContext(UserContext);

  const { _id, title, readTime, slug, tag, author, comments, likes, postedOn } =
    blogs;

  const userLiked = likes.find((like) => {
    return like.userId === user.id;
  });

  const likeCount = likes.length;
  const commentCount = comments.length;

  return (
    <motion.div
      className="blog-list"
      transition={{ duration: 0.4 }}
      whileHover={{ scale: 1.0025 }}
      whileTap={{ scale: 0.9975 }}
      style={{ x: 0 }}
    >
      <Link to={`/blog/${slug}`}></Link>
      <div className="blog-header">
        <h3 className="blog-title">{title}</h3>
        <span className="author">{author}</span>
      </div>

      <div className="blog-details">
        <div className="blog-tags">
          <span className="blog-tag">{tag}</span>
          <p className="blog-time">{readTime}</p>
        </div>
        <div className="blog-date">{moment(postedOn).format("MMM YYYY")}</div>
      </div>

      <div className="blog-footer">
        <div className="blog-likes" onClick={() => toggleLike(_id)}>
          {userLiked === undefined ? (
            <i className="bx bx-like"></i>
          ) : (
            <i className="bx bxs-like"></i>
          )}
          <span>{likeCount}</span>
        </div>
        <div className="blog-comment">
          <span>{commentCount}</span>
          <i className="bx bx-message-dots"></i>
        </div>
      </div>
    </motion.div>
  );
}
