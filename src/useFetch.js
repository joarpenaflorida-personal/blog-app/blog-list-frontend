import { useEffect, useState } from "react";

const useFetch = (url) => {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);
  const [isPending, setIsPending] = useState(true);

  useEffect(() => {
    const abortCont = new AbortController();

    setTimeout(() => {
      fetch(
        url,
        {
          headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
        },
        { signal: abortCont.signal }
      )
        .then((res) => {
          if (!res.ok) {
            throw Error("Could not fetch the data for that resource.");
          }
          return res.json();
        })
        .then((data) => {
          setData(data);
          setIsPending(false);
          setError(null);
        })
        .catch((err) => {
          if (err.name === "AbortError") {
            console.log("Fetch aborted");
          } else {
            setIsPending(false);
            setError(err.message);
          }
        });
    }, 100);

    return () => abortCont.abort();
  }, [url]);
  return { data, isPending, error };
};

export default useFetch;
