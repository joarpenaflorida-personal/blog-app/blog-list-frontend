import "./login.css";
import { Link, Navigate, useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";
import { useContext } from "react";
import UserContext from "../UserContext";
import { ToastContainer, toast } from "react-toastify";
import { motion } from "framer-motion";
import "react-toastify/dist/ReactToastify.css";
const validator = require("validator");

export default function Login() {
  const navigate = useNavigate();
  const [isActive, setIsActive] = useState(false);
  const initialState = {
    email: "",
    password: "",
  };
  const { user, setUser } = useContext(UserContext);
  const [formValues, setFormValues] = useState(initialState);
  const [formErrors, setFormErrors] = useState([]);
  const [showPassword, setShowPassword] = useState(false);

  function showToastSuccess() {
    toast.success("Login Successful...", {
      position: toast.POSITION.TOP_CENTER,
    });
  }
  function showToastNoUser() {
    toast.info("Login Failed. Check your login details and try again.", {
      position: toast.POSITION.TOP_CENTER,
    });
  }

  const resolveWait = new Promise((resolve) => setTimeout(resolve, 1000));
  function showToastLoading() {
    toast.promise(
      resolveWait,
      {
        pending: "Logging in...",
        success: "Verifying User Credentials",
        error: "Login failed.",
      },
      {
        position: toast.POSITION.TOP_CENTER,
      }
    );
  }

  function showToastError() {
    toast.error("Something went wrong. Registration failed...", {
      position: toast.POSITION.TOP_CENTER,
    });
  }

  const error = {
    color: "rgb(253, 49, 49)",
    fontWeight: 500,
  };

  const errorBorder = {
    borderColor: "rgb(253, 49, 49)",
  };

  function handleChange(e) {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  }
  useEffect(() => {
    if (Object.keys(formErrors).length === 0) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [formErrors]);

  function validate(values) {
    let errors = {};
    if (!values.email) {
      errors.email = "Email is required.";
    } else if (!validator.isEmail(values.email)) {
      errors.email = "Email Address is not valid.";
    }
    if (!values.password) {
      errors.password = "Password is required.";
    }
    if (values.password.length < 6) {
      errors.password = "Password must be at least 6 Characters";
    }
    return errors;
  }

  function handleSubmit(e) {
    e.preventDefault();
    setFormErrors(validate(formValues));
    const { email, password } = formValues;

    if (Object.keys(formErrors).length === 0 && isActive) {
      showToastLoading();
      fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: email,
          password: password,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          if (data.accessToken !== undefined) {
            navigate("/blogs");

            localStorage.setItem("token", data.accessToken);
            retrieveUserDetails(data.accessToken);
            showToastSuccess();
          } else {
            showToastNoUser();
          }
        });
    }
  }

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
            username: data.username,
            email: data.email,
          });
        }
      });
  };

  const toggleShowPass = () => {
    setShowPassword((prevState) => !prevState);
  };
  return user.id !== null ? (
    <Navigate to="/blogs" end />
  ) : (
    <>
      <ToastContainer />
      <motion.div
        className="form-container"
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
        transition={{ duration: 0.3 }}
      >
        <form onSubmit={(e) => handleSubmit(e)}>
          <h1 className="logs">logs</h1>
          <div className="form-group">
            <label htmlFor="email">Email Address</label>
            <input
              type="email"
              name="email"
              maxLength={60}
              style={formErrors.email && errorBorder}
              value={formValues.email}
              onChange={handleChange}
              className="form-input"
              placeholder="Enter Email Address"
            />
            {<p style={formErrors.email && error}>{formErrors.email}</p>}
          </div>
          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input
              type={showPassword ? "text" : "password"}
              name="password"
              maxLength={64}
              style={formErrors.password && errorBorder}
              value={formValues.password}
              onChange={handleChange}
              className="form-input"
              placeholder="Enter Password"
            />
            <div className="show-toggle" onClick={toggleShowPass}>
              {showPassword ? (
                <i className="bx bxs-hide"></i>
              ) : (
                <i className="bx bxs-show"></i>
              )}
            </div>
            {<p style={formErrors.password && error}>{formErrors.password}</p>}
          </div>

          <div className="form-group">
            <button type="submit" className="login">
              Login
            </button>
          </div>
          <div className="form-group end">
            <p>
              Not yet registered? <Link to="/register">Register</Link>
            </p>
          </div>
        </form>
      </motion.div>
    </>
  );
}
