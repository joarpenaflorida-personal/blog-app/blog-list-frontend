import { Navigate } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import UserContext from "../UserContext";
import BlogList from "../components/BlogList";
import UserBlogs from "../components/UserBlogs";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./blogs.css";
import { motion } from "framer-motion";

export default function Blogs() {
  const initialState = {
    title: "",
    description: "",
    tag: "",
  };
  const initialTag = {
    tagState: "",
  };
  const { user } = useContext(UserContext);

  const [blogs, setBlogs] = useState([]);
  const [filtered, setFiltered] = useState([]);
  const [isFiltered, setIsFiltered] = useState(false);
  const [userBlogs, setUserBlogs] = useState([]);
  const [error, setError] = useState(false);
  const [isPending, setIsPending] = useState(true);
  const [toggleEdit, setToggleEdit] = useState(false);
  const [toggleShow, setToggleShow] = useState(false);
  const [formErrors, setFormErrors] = useState([]);

  const [formValues, setFormValues] = useState(initialState);
  const [tagValue, setTagValue] = useState(initialTag);

  const [toggleHome, setToggleHome] = useState(true);
  const [toggleUser, setToggleUser] = useState(false);
  const [blogId, setBlogId] = useState("");
  const [toggleShowData, setToggleShowData] = useState(true);
  const [searchInput, setSearchInput] = useState("");

  function showToastError() {
    toast.error("Post Submission failed...", {
      position: toast.POSITION.TOP_CENTER,
      hideProgressBar: false,
    });
  }
  function showToastDelete() {
    toast.error("Post deletion failed...", {
      position: toast.POSITION.TOP_CENTER,
      hideProgressBar: true,
      isLoading: true,
    });
  }
  function showPromise() {
    toast.promise(
      resolveWait,
      {
        pending: "Verifying form.",
        success: "Success.",
      },
      {
        position: toast.POSITION.TOP_CENTER,
      }
    );
  }
  const resolveWait = new Promise((resolve) => setTimeout(resolve, 1000));

  function showPromiseDelete() {
    toast.promise(
      resolveWait,
      {
        pending: "Deleting post.",
        success: "Post deletion successful!",
        error: "Post submisssion failed. Please try again.",
      },
      {
        position: toast.POSITION.TOP_CENTER,
      }
    );
  }

  const errors = {
    color: "rgb(253, 49, 49)",
  };
  const errorBorder = {
    borderColor: "rgb(253, 49, 49)",
  };

  function toggleChangeHome() {
    setToggleHome(true);
    setToggleUser(false);
    setTagValue(initialTag);
    setSearchInput("");
  }
  function toggleChangeUser() {
    setToggleUser(true);
    setToggleHome(false);
    setTagValue(initialTag);
    setSearchInput("");
  }

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    if (tagValue.tagState !== "") {
      setIsFiltered(true);
      setFiltered(
        blogs.filter((blog) => {
          return blog.props.blogs.tag
            .toLowerCase()
            .includes(tagValue.tagState.toLowerCase());
        })
      );
      setFiltered(
        userBlogs.filter((blog) => {
          return blog.props.blogs.tag
            .toLowerCase()
            .includes(tagValue.tagState.toLowerCase());
        })
      );
    } else {
      setIsFiltered(false);
      fetchData();
    }
  }, [tagValue]);

  useEffect(() => {
    if (searchInput.length > 0) {
      setBlogs(
        blogs.filter((blog) => {
          const blogs = blog.props.blogs.title
            .toLowerCase()
            .includes(searchInput.toLowerCase());
          return blogs;
        })
      );
      setUserBlogs(
        userBlogs.filter((blog) => {
          const blogs = blog.props.blogs.title
            .toLowerCase()
            .includes(searchInput.toLowerCase());
          return blogs;
        })
      );
    } else {
      fetchData();
    }
  }, [searchInput]);

  function fetchData() {
    fetch(`${process.env.REACT_APP_API_URL}/blogs/blog/`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setBlogs(
            data.map((blog) => {
              return <BlogList blogs={blog} key={blog._id} />;
            })
          );
        } else {
          setError("No blog found.");
        }
        setIsPending(false);
      });

    fetch(`${process.env.REACT_APP_API_URL}/blogs/blog/user`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setUserBlogs(
            data.map((blog) => {
              return (
                <UserBlogs
                  blogs={blog}
                  key={blog._id}
                  deleteBlog={deleteBlog}
                  updateBlog={updateBlog}
                  archive={archive}
                />
              );
            })
          );
        }
        setIsPending(false);
      })
      .catch((err) => console.log(err));
    setToggleShowData(true);
  }
  function validate(values) {
    const errors = {};

    if (!values.title) {
      errors.title = "Title is required!";
    }
    if (!values.description) {
      errors.description = "Blog Description is required!";
    }
    if (!values.tag) {
      errors.tag = "Category is required!";
    }
    return errors;
  }

  function addPost(e) {
    e.preventDefault();
    showPromise();
    setFormErrors(validate(formValues));
    if (Object.keys(formErrors).length === 0) {
      fetch(`${process.env.REACT_APP_API_URL}/blogs/`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          title: formValues.title,
          description: formValues.description,
          tag: formValues.tag,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          if (data) {
            toggleFormAdd();
            fetchData();
          } else {
          }
        })
        .catch((err) => console.log(err));
    }
  }

  function updateBlog(slug) {
    toggleFormEdit();

    fetch(`${process.env.REACT_APP_API_URL}/blogs/blog/${slug}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setBlogId(data._id);
          setFormValues(data);
        } else {
          setFormValues(initialState);
        }
      });
  }
  function updatePost(e) {
    e.preventDefault();
    showPromise();
    setFormErrors(validate(formValues));
    if (Object.keys(formErrors).length === 0 && blogId) {
      fetch(`${process.env.REACT_APP_API_URL}/blogs/${blogId}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          title: formValues.title,
          description: formValues.description,
          tag: formValues.tag,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          if (data) {
            toggleFormEdit();
            fetchData();
          } else {
            showToastError();
          }
        });
    }
  }

  function archive(id, status) {
    if (id) {
      fetch(`${process.env.REACT_APP_API_URL}/blogs/${id}`, {
        method: "PATCH",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          isActive: !status,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          if (data) {
            fetchData();
          } else {
            showToastDelete();
          }
        })
        .catch((err) => console.log(err));
    }
  }
  function deleteBlog(id) {
    showPromiseDelete();

    if (id) {
      fetch(`${process.env.REACT_APP_API_URL}/blogs/${id}`, {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          if (data) {
            fetchData();
          } else {
            showToastDelete();
          }
        });
    }
  }

  function toggleFormAdd() {
    setToggleShow((prevState) => !prevState);
    if (toggleShow) {
      setToggleShowData(true);
    } else {
      setToggleShowData(false);
    }
    setToggleEdit(false);
    setFormValues(initialState);
  }
  function toggleFormEdit() {
    setToggleEdit((prevState) => !prevState);
    if (toggleEdit) {
      setToggleShowData(true);
    } else {
      setToggleShowData(false);
    }
    setToggleShow(false);
    setFormValues(initialState);
  }

  function handleChange(e) {
    const { name, value, checked, type } = e.target;
    setFormValues((formValues) => ({
      ...formValues,
      [name]: type === "checkbox" ? checked : value,
    }));
  }

  function handleTagChange(e) {
    e.preventDefault();
    const { name, value, checked, type } = e.target;
    setTagValue((tagValue) => ({
      ...tagValue,
      [name]: type === "checkbox" ? checked : value,
    }));
  }

  function handleSearch(e) {
    e.preventDefault();
    setSearchInput(e.target.value);
  }

  return user.id === null ? (
    <Navigate to="/" />
  ) : (
    <>
      <ToastContainer />
      <div className="blog-container">
        <div className="sidebar">
          <ul className="side-bar-links">
            <li onClick={toggleChangeHome}>
              <i className="bx bx-home-alt-2"></i>
            </li>
            <li onClick={toggleChangeUser}>
              <i className="bx bxl-blogger"></i>
            </li>
          </ul>
        </div>

        {toggleHome && (
          <motion.div
            className="blog-main"
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
            transition={{ duration: 0.3 }}
          >
            {blogs.length > 0 ? <h2>Blogs</h2> : <h2>No blog found.</h2>}
            {error && <div>{error}</div>}
            {isPending && <div>Loading...</div>}
            {isFiltered ? <>{filtered}</> : <> {toggleShowData && blogs}</>}
          </motion.div>
        )}
        {toggleUser && (
          <motion.div
            className="blog-main"
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
            transition={{ duration: 0.3 }}
          >
            <div className="blog-header">
              <p>{user.username}</p>
              <i
                className="bx bx-plus add"
                onClick={toggleFormAdd}
                style={{ display: toggleEdit ? "none" : "block" }}
              ></i>
            </div>
            {userBlogs.length > 0 ? (
              <h2>User Blogs</h2>
            ) : (
              <h2>No user blog found.</h2>
            )}
            {isFiltered ? <>{filtered}</> : <> {toggleShowData && userBlogs}</>}
            {toggleShow && (
              <div className="blog-form">
                <form onSubmit={addPost}>
                  <h1 className="logs">Post Blog</h1>
                  <div className="close" onClick={toggleFormAdd}>
                    <i className="bx bx-x"></i>
                  </div>
                  <div className="form-group">
                    <label htmlFor="title">Title</label>
                    <input
                      type="text"
                      name="title"
                      style={formErrors.title && errorBorder}
                      value={formValues.title}
                      maxLength={60}
                      onChange={handleChange}
                      className="form-input"
                      placeholder="Enter Title"
                    />
                    {<p style={errors}>{formErrors.title}</p>}
                  </div>
                  <div className="form-group">
                    <label htmlFor="description">Description</label>
                    <textarea
                      name="description"
                      style={formErrors.description && errorBorder}
                      value={formValues.description}
                      maxLength={3000}
                      onChange={handleChange}
                      className="form-input"
                      placeholder="Enter Description"
                    ></textarea>
                    {<p style={errors}>{formErrors.description}</p>}
                  </div>
                  <div className="form-group">
                    <label htmlFor="tag">Category</label>
                    <select
                      name="tag"
                      onChange={handleChange}
                      value={formValues.tag}
                      style={formErrors.tag && errorBorder}
                    >
                      <option value="">--</option>
                      <option value="Web Development">Web Development</option>
                      <option value="Sci-Fi">Sci-Fi</option>
                      <option value="Technology">Technology</option>
                    </select>
                    {<p style={errors}>{formErrors.description}</p>}
                  </div>
                  <div className="form-group">
                    <input type="submit" />
                  </div>
                </form>
              </div>
            )}

            {toggleEdit && (
              <div className="blog-form">
                <form onSubmit={updatePost}>
                  <h1 className="logs">Edit Blog</h1>
                  <div className="close" onClick={toggleFormEdit}>
                    <i className="bx bx-x"></i>
                  </div>
                  <div className="form-group">
                    <label htmlFor="title">Title</label>
                    <input
                      type="text"
                      name="title"
                      style={formErrors.title && errorBorder}
                      value={formValues.title}
                      maxLength={60}
                      onChange={handleChange}
                      className="form-input"
                      placeholder="Enter Title"
                    />
                    {<p style={errors}>{formErrors.title}</p>}
                  </div>
                  <div className="form-group">
                    <label htmlFor="description">Description</label>
                    <textarea
                      name="description"
                      style={formErrors.description && errorBorder}
                      value={formValues.description}
                      maxLength={3000}
                      onChange={handleChange}
                      className="form-input"
                      placeholder="Enter Description"
                    ></textarea>
                    {<p style={errors}>{formErrors.description}</p>}
                  </div>
                  <div className="form-group">
                    <label htmlFor="tag">Category</label>
                    <select
                      name="tag"
                      onChange={handleChange}
                      value={formValues.tag}
                      style={formErrors.tag && errorBorder}
                    >
                      <option value="">--</option>
                      <option value="Web Development">Web Development</option>
                      <option value="Sci-Fi">Sci-Fi</option>
                      <option value="Technology">Technology</option>
                    </select>
                    {<p style={errors}>{formErrors.description}</p>}
                  </div>
                  <div className="form-group">
                    <input type="submit" />
                  </div>
                </form>
              </div>
            )}
          </motion.div>
        )}

        <div className="featured">
          <div className="featured-search">
            <input
              type="text"
              placeholder="Search..."
              onChange={handleSearch}
            />
            <i className="bx bx-search"></i>
          </div>
          <div className="featured-category">
            <label htmlFor="tag">Category</label>
            <select
              value={tagValue.tagState}
              onChange={handleTagChange}
              name="tagState"
            >
              <option value="">All</option>
              <option value="Web Development">Web Development</option>
              <option value="Sci-Fi">Sci-Fi</option>
              <option value="Technology">Technology</option>
            </select>
          </div>
        </div>
      </div>
    </>
  );
}
