import { useParams } from "react-router-dom";
import UserContext from "../UserContext";
import { useContext, useEffect, useState } from "react";
import Comment from "../components/Comments";
import moment from "moment";
import "./blogview.css";
import { motion } from "framer-motion";
export default function BlogView() {
  const { user } = useContext(UserContext);

  const [comments, setComments] = useState([]);
  const [likes, setLikes] = useState(0);
  const [userLiked, setUserLiked] = useState(false);
  const [toggleShow, setToggleShow] = useState(false);

  const initialState = {
    comment: "",
  };

  const initialBlog = {
    author: "",
    id: "",
    postedOn: "",
    readTime: "",
    tag: "",
    title: "",
    description: "",
  };
  const [blog, setBlog] = useState(initialBlog);
  const [formValue, setFormValue] = useState(initialState);

  let { slug } = useParams();

  useEffect(() => {
    fetchData();
  }, []);

  function handleChange(e) {
    const { name, value } = e.target;
    setFormValue({ ...formValue, [name]: value });
  }

  function submitComment(e) {
    e.preventDefault();
    console.log(user);
    if (formValue.comment !== "") {
      fetch(`${process.env.REACT_APP_API_URL}/blogs/comment/${blog.id}`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          comment: formValue.comment,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          if (data) {
            console.log(data);
            fetchData();
            setFormValue(initialState);
          }
        });
    }
  }

  function handleSubmitUpdate(e, id, dataComment, toggleEdit) {
    e.preventDefault();
    fetch(
      `${process.env.REACT_APP_API_URL}/blogs/comment/update/${blog.id}/${id}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          comment: dataComment,
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          toggleEdit = !toggleEdit;
          fetchData();
        }
      });
  }
  function handleDelete(id) {
    fetch(
      `${process.env.REACT_APP_API_URL}/blogs/comment/delete/${blog.id}/${id}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          fetchData();
        }
      })
      .catch((err) => console.log(err));
  }
  function toggleClick() {
    setToggleShow((prevState) => !prevState);
  }
  function toggleLike() {
    console.log(blog.id);
    fetch(`${process.env.REACT_APP_API_URL}/blogs/blog/like/${blog.id}`, {
      method: "POST",
      headers: {
        "Content-Type": "application.json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          fetchData();
        }
      });
  }
  function fetchData() {
    fetch(`${process.env.REACT_APP_API_URL}/blogs/blog/${slug}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((blog) => {
        if (blog) {
          const { author, _id, postedOn, readTime, tag, title, description } =
            blog;

          const userLiked = blog.likes.find((like) => {
            return like.userId === user.id;
          });

          setUserLiked(userLiked);
          setLikes(blog.likes.length);

          const sorted = blog.comments.sort(function (a, b) {
            return new Date(b.commentedOn) - new Date(a.commentedOn);
          });

          setComments(
            sorted.map((comment) => {
              return (
                <Comment
                  blogId={comment.blogId}
                  comments={comment}
                  key={comment._id}
                  handleSubmitUpdate={handleSubmitUpdate}
                  handleDelete={handleDelete}
                />
              );
            })
          );

          setBlog({
            author: author,
            id: _id,
            postedOn: postedOn,
            readTime: readTime,
            tag: tag,
            title: title,
            description: description,
          });
        }
      });

    console.log(userLiked);
  }
  return (
    <>
      {blog ? (
        <motion.div
          className="blog-view-container"
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
          transition={{ duration: 0.4 }}
        >
          <div className="blog">
            <div className="blog-view-header">
              <h2 className="title">{blog.title}</h2>
              <div className="blog-view-details">
                <span className="tag">{blog.tag}</span>

                <span className="read-time">{blog.readTime}</span>
              </div>
            </div>
            <p className="blog-paragraph">{blog.description}</p>
          </div>

          <div className="blog-author">
            <div className="blog-likes" onClick={toggleLike}>
              {userLiked === undefined ? (
                <>
                  <i className="bx bx-like"></i>
                </>
              ) : (
                <>
                  <i className="bx bxs-like"></i>
                </>
              )}
              <span>{likes}</span>
            </div>
            <div className="authors">
              <span className="author">{blog.author}</span>
              <p>{moment(blog.postedOn).format("MMM YYYY")}</p>
            </div>
          </div>

          <div className="comment">
            <div className="comment-header " onClick={toggleClick}>
              <div className="comments-top">
                <p>Comments</p>
                <span>{comments.length}</span>
              </div>

              <div className="toggle">
                <i className="bx bx-chevron-down"></i>
              </div>
            </div>
            <div className="comment-container">
              {toggleShow ? (
                <>
                  <form onSubmit={submitComment}>
                    <input
                      type="text"
                      maxLength={55}
                      className="comment-input"
                      placeholder="Add a comment..."
                      name="comment"
                      value={formValue.comment}
                      onChange={handleChange}
                    />
                  </form>
                  <>{comments} </>
                </>
              ) : (
                <></>
              )}
            </div>
          </div>
        </motion.div>
      ) : (
        <>
          <h1>No Blogs found.</h1>
        </>
      )}
    </>
  );
}
