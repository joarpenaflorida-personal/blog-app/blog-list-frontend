import { Link, Navigate, useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";
import "./register.css";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { motion } from "framer-motion";
const validator = require("validator");

export default function Register() {
  const navigate = useNavigate();

  function ShowToastSuccess() {
    toast.success("Registration Successful...", {
      position: toast.POSITION.TOP_CENTER,
      hideProgressBar: true,
      isLoading: true,
    });
  }
  function showToastDuplicate() {
    toast.info("Email Duplicate.", {
      position: toast.POSITION.TOP_CENTER,
    });
  }
  function showToastError() {
    toast.error("Something went wrong. Registration failed.", {
      position: toast.POSITION.TOP_CENTER,
    });
  }
  const resolveWait = new Promise((resolve) => setTimeout(resolve, 1000));
  function showPromise() {
    toast.promise(
      resolveWait,
      {
        pending: "Registering...",
        success: "Verifying User Information...",
        error: "Registration failed. Please try again.",
      },
      {
        position: toast.POSITION.TOP_CENTER,
      }
    );
  }
  const initialValues = {
    username: "",
    email: "",
    password: "",
    cpassword: "",
  };
  const [showPassword, setShowPassword] = useState(false);
  const [cShowPassword, setCShowPassword] = useState(false);
  const [isActive, setIsActive] = useState(false);
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmit, setIsSubmit] = useState(false);

  const toggleShowPass = () => {
    setShowPassword((prevState) => !prevState);
  };
  const toggleShowCPass = () => {
    setCShowPassword((prevState) => !prevState);
  };
  function validate(values) {
    const errors = {};

    const passwordValidate =
      /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{6,}$/;

    if (!values.username) {
      errors.username = "Username is required!";
    } else if (values.username.length < 4) {
      errors.username = "Username must at least be 4 characters.";
    }
    if (!values.email) {
      errors.email = "Email is required!";
    } else if (!validator.isEmail(values.email)) {
      errors.email = "Email Address is not valid.";
    }
    if (!values.password) {
      errors.password = "Password is required!";
    } else if (!passwordValidate.test(values.password)) {
      errors.password = `Password must have:

      at least 6 Characters.
      at least 1 Special Character
      at least 1 Number
      at least 1 Uppercase Letter
      at least 1 Lowercase Letter.`;
    }
    if (!values.cpassword) {
      errors.cpassword = "Confirm Password is required!";
    } else if (values.password !== values.cpassword) {
      errors.cpassword = "Password does not match.";
    }
    return errors;
  }

  useEffect(() => {
    if (Object.keys(formErrors).length === 0) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [formErrors, isActive]);
  function handleChange(e) {
    const { name, value } = e.target;
    setFormValues({
      ...formValues,
      [name]: value,
    });
  }
  function handleSubmit(e) {
    e.preventDefault();
    showPromise();
    setFormErrors(validate(formValues));
    if (Object.keys(formErrors).length === 0 && isActive) {
      showPromise();
      fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: formValues.email,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          if (data) {
            showToastDuplicate();
            setFormErrors({
              ...formErrors,
              email: "Duplicate Email Address.",
            });
          } else {
            fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify({
                username: formValues.username,
                email: formValues.email,
                password: formValues.password,
              }),
            })
              .then((res) => res.json())
              .then((data) => {
                console.log(data);
                if (data) {
                  ShowToastSuccess();

                  setTimeout(() => {
                    setFormValues(initialValues);

                    navigate("/");
                  }, 3000);
                } else {
                  showToastError();
                }
              });
          }
        });
    }
  }

  const errorBorder = {
    borderColor: "rgb(253, 49, 49)",
  };

  const error = {
    color: "rgb(253, 49, 49)",
    fontWeight: "500",
  };
  return (
    <>
      <ToastContainer />
      <motion.div
        className="form-container"
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
        transition={{ duration: 0.3 }}
      >
        <form onSubmit={(e) => handleSubmit(e)}>
          <h1 className="logs">logs</h1>
          <div className="form-group">
            <label htmlFor="email">Username</label>
            <input
              type="text"
              name="username"
              style={formErrors.username && errorBorder}
              value={formValues.username}
              maxLength={60}
              onChange={handleChange}
              className="form-input"
              placeholder="Enter Username"
            />
            {<p style={error}>{formErrors.username}</p>}
          </div>
          <div className="form-group">
            <label htmlFor="email">Email Address</label>
            <p>We won't share your email address.</p>
            <input
              type="email"
              name="email"
              style={formErrors.email && errorBorder}
              value={formValues.email}
              maxLength={60}
              onChange={handleChange}
              className="form-input"
              placeholder="Enter Email Address"
            />
            <div className="show-toggle"></div>

            {<p style={error}>{formErrors.email}</p>}
          </div>
          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input
              type={showPassword ? "text" : "password"}
              name="password"
              maxLength={64}
              style={formErrors.password && errorBorder}
              value={formValues.password}
              onChange={handleChange}
              className="form-input"
              placeholder="Enter Password"
            />
            <div className="show-toggle" onClick={toggleShowPass}>
              {showPassword ? (
                <i className="bx bxs-hide"></i>
              ) : (
                <i className="bx bxs-show"></i>
              )}
            </div>
            {<pre style={error}>{formErrors.password}</pre>}
          </div>
          <div className="form-group">
            <label htmlFor="cpassword">Confirm Password</label>
            <input
              type={cShowPassword ? "text" : "password"}
              name="cpassword"
              maxLength={64}
              style={formErrors.password && errorBorder}
              value={formValues.cpassword}
              onChange={handleChange}
              className="form-input"
              placeholder="Confirm Password"
            />
            <div className="show-toggle" onClick={toggleShowCPass}>
              {cShowPassword ? (
                <i className="bx bxs-hide"></i>
              ) : (
                <i className="bx bxs-show"></i>
              )}
            </div>
            {<p style={error}>{formErrors.cpassword}</p>}
          </div>

          <div className="form-group">
            <button type="submit" className="login">
              Register
            </button>
          </div>
          <div className="form-group end">
            <p>
              Already Registered? <Link to="/">Login</Link>
            </p>
          </div>
        </form>
      </motion.div>
    </>
  );
}
