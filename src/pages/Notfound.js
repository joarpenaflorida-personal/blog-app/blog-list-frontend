import { useContext } from "react";
import UserContext from "../UserContext";
import { Link } from "react-router-dom";
export default function NotFound() {
  const { user } = useContext(UserContext);

  return (
    <>
      {user.id === null ? (
        <div className="notfound">
          <h2>404</h2>
          <p>Page not found.</p>
          <Link to="/">Back to Login</Link>
        </div>
      ) : (
        <div className="notfound">
          <h2>404</h2>
          <p>Page not found.</p>
          <Link to="/blogs">Back to Blogs</Link>
        </div>
      )}
    </>
  );
}
