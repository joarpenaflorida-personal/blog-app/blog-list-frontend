import "./App.css";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";
// components
import NavBar from "./components/NavBar";
// pages
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Register from "./pages/Register";
import BlogView from "./pages/BlogView";
// react import
import { useState, useEffect } from "react";
// useContext = user provider
import { UserProvider } from "./UserContext";
import Blogs from "./pages/Blogs";
import NotFound from "./pages/Notfound";
// components

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
    username: null,
    email: null,
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    fetchData();
  }, []);

  function fetchData() {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data._id !== undefined) {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
            email: data.email,
            username: data.username,
          });
        } else {
          setUser({
            id: null,
            isAdmin: data.isAdmin,
            username: null,
            email: null,
          });
        }
      });
  }

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <div className="container">
          <div className="header">
            <NavBar />
          </div>
          <div className="contents">
            <Routes>
              {user.id !== null ? (
                user.isAdmin ? (
                  <>
                    <Route path="/dashboard" />
                    <Route path="/logout" element={<Logout />} />
                  </>
                ) : (
                  <>
                    <Route exact path="/blogs" element={<Blogs />} />
                    <Route path="/blog/:slug" element={<BlogView />} />
                    <Route path="/logout" element={<Logout />} />
                  </>
                )
              ) : (
                <>
                  <Route path="/" element={<Login />} />
                  <Route path="/register" element={<Register />} />
                </>
              )}
              <Route path="*" element={<NotFound />} />
            </Routes>
          </div>
        </div>
      </Router>
    </UserProvider>
  );
}

export default App;
